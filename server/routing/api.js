const express = require('express');
const router = express.Router();
const api = require('../controllers/api');


router.get('/',function (req,res) {
    res.send('api works');
});

router.get('/test',function (req,res) {
   api.test(req,res);
});

router.get('/sql',function (req,res) {
    api.sql(req,res);
});

router.get('/torte',function(req,res){
    api.torte(req,res);
});

router.get('/torte/:id',function (req,res) {
    api.idtorte(req,res);
});

router.get('/frontmenu',function (req,res,next) {
    api.getFrontMenu(req,res);
});
router.get('/getlistcake/:sub',function (req,res,next) {
    api.getsubcake(req,res);
});
router.get('/getcakes/:cake',function (req,res,next) {
    api.getcake(req,res);
});

router.get('/singlecake/:group',function (req,res,next) {
    api.singlecakes(req,res);
});

router.get('/getprice/:cake',function (req,res,next) {
    api.getprice(req,res);
});

router.get('/gettaste',function (req,res,next) {
    api.gettaste(req,res);
});

router.get('/gettaste/:id',function (req,res,next) {
    api.getsingtaste(req,res);
});




module.exports = router;
