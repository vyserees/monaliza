const exec = require('../controllers/exec');
const async = require('async');


exports.test = function (req,res) {
  res.send('Its Works');
};

exports.sql = function (req,res) {
  exec.custom('SELECT * FROM cakes',function (data) {
      res.send(data);
  });
};

exports.torte = function (req,res) {
    var sql = "SELECT A.name";
};

exports.idtorte = function (req,res) {
  res.send('ok');
};

exports.getFrontMenu = function (req,res) {
    exec.custom('SELECT * FROM groups WHERE parent = 0',function (data) {
        // res.send(data);
        async.forEachOf(data,function (v,i,c) {
            exec.custom("SELECT * FROM groups WHERE parent = "+ data[i].id,function (data2) {
                data[i].children = data2;
                c(null);
            });
        },function (err) {
            if(err){throw err;}

            res.json(data);
        });
    });
};
exports.getcake = function (req,res) {
    async.auto({
        cake: function (call) {
            exec.custom("SELECT * FROM cakes WHERE slug='"+ req.params.cake + "'",function (data){
                call(null,data[0]);
            });
        },
        ing: ['cake',function (arg,call) {
            var sql = "SELECT A.*,B.quantity,C.name AS iname,D.name AS uname FROM recepies AS A ";
            sql+= "INNER JOIN igridient_recepie AS B ON A.id=B.recepie_id JOIN ingridients AS C ON B.ingridient_id = C.id ";
            sql+= "JOIN units AS D ON C.unit_id = D.id WHERE A.cake_id="+ arg.cake.id;
            exec.custom(sql ,function (data) {
                call(null,data);
            });
        }]
    },function (err,result) {
        console.log(result);
        res.json({
            cake: result.cake.name,
            ing: result.ing
        });
    });
};


exports.getsubcake = function (req,res) {
    async.auto({
        group: function (call) {
            exec.custom("SELECT * FROM groups WHERE slug='"+ req.params.sub+ "'",function (data){
                call(null,data[0]);
            });
        },
        cakes: ['group',function (arg,call) {
            var gid = arg.group.id;
            console.log(gid);
            exec.custom("SELECT A.*,B.value,C.price FROM cakes AS A INNER JOIN cake_weights AS B ON A.id=B.cake_id JOIN cake_prices AS C ON A.id=C.cake_id WHERE A.group_id ="+ gid ,function (data) {
               call(null,data);
            });
        }]
    },function (err,result) {
        console.log(result);
        res.json({
            page_title: result.group.name,
            cakes: result.cakes
        });
    });
};

exports.singlecakes = function (req,res) {
    exec.custom("SELECT cakes.name FROM cakes INNER JOIN groups ON cakes.group_id=groups.id WHERE groups.slug='"+req.params.group+"'",function (data) {
    });
};

exports.getprice = function (req,res) {
    sql = "SELECT * FROM cakes AS A INNER JOIN cake_weights AS B ON A.id=B.cake_id JOIN cake_prices AS C ";
    sql+= "ON A.id=C.cake_id WHERE A.slug='"+req.params.cake+"'";
  exec.custom(sql,function (data) {

      res.send(data);
  })
};

exports.gettaste = function (req,res,next) {
  exec.custom("SELECT * FROM tastes",function (data) {
      res.json(data);
  });
};

exports.getsingtaste = function (req,res,next) {
    exec.custom("SELECT * FROM tastes WHERE id="+req.params.id,function (data) {
        console.log(data);
        res.json(data[0]);
    });
};