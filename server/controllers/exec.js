var db = require('../controllers/db');

exports.custom = function(q,callback){
    db(function(conn){
        conn.query(q,function(err,result,fields){
            if (err) {
                throw err;
            }
            conn.release();
            callback(result);
        });
    });
};

/*CHANGING*/
exports.insert = function(table,ins,callback){
    query = "INSERT INTO " + table + " SET ?";
    db(function(conn){
        conn.query(query,ins,function(err,result,fields){
            if (err) {
                throw err;
            }
            conn.release();
            callback(result);
        });
    });
};

exports.update = function(table,ups,whs,callback){
    var set_ups = set_update_ins(ups);
    var set_whs = set_update_whs(whs);
    var esc = set_ups.arr.concat(set_whs.arr);
    var query = "UPDATE " + table + " SET " + set_ups.str + " WHERE " + set_whs.str;
    db(function(conn){
        conn.query(query,esc,function(err,result,fields){
            if (err) {
                throw err;
            }
            conn.release();
            callback(result);
        });
    });
};

/**/
var set_update_ins = function(ups){
    var arr = [];
    var str = [];
    for(var key in ups){
        str.push(key + "=?");
        arr.push(ups[key]);
    }
    return {str:str.join(','),arr:arr};
};

/**/
var set_update_whs = function(whs){
    var arr = [];
    var str = [];
    for(var key in whs){
        str.push(key + "=?");
        arr.push(whs[key]);
    }
    return {str:str.join(','),arr:arr};
};