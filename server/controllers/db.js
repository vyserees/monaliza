var mysql = require('mysql');
var cnf = require('../controllers/conf');
var pool = mysql.createPool({
    connectionLimit:10,
    host:cnf.DB_HOST,
    user:cnf.DB_USER,
    password:cnf.DB_PASS,
    //password:'vn832!?Ak#',
    database:cnf.DB_NAME,
    multipleStatements:true
});

module.exports = function conn(callback){
    pool.getConnection(function(err,connection){
        if(err){
            throw err;
        }
        callback(connection);
    });
};