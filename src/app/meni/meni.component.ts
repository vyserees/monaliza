import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute} from '@angular/router';
import {CakeServiceService} from '../cake-service.service';


@Component({
  selector: 'app-meni',
  templateUrl: './meni.component.html',
  styleUrls: ['./meni.component.css']
})
export class MeniComponent implements OnInit {
  public menus;
  public subgroup;
  public cakes;
  constructor(private http: HttpClient,
              private route: ActivatedRoute) { }

  ngOnInit() {
      this.subgroup = this.route.snapshot.paramMap.get('subgroup');
    this.http.get('/api/frontmenu').subscribe(data => this.menus = data);
    this.http.get('/api/singlecake/' + this.subgroup).subscribe(data => this.cakes = data);
  }



}
