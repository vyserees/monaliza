import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MeniComponent} from './meni/meni.component';
import { SinglecakeComponent} from './singlecake/singlecake.component';
import {CakelistComponent} from './cakelist/cakelist.component';


const routes: Routes = [
    {path: 'app', component: AppComponent},
    {path: ':group/:subgroup/:cake', component: SinglecakeComponent},
    {path: ':group/:subgroup', component: CakelistComponent},
    {path: ':group', component: CakelistComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
