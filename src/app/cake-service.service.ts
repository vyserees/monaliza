import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CakeServiceService {

  constructor(private http: HttpClient) { }
  getcake(cake) {
      return this.http.get('/api/getlistcake/' + cake);
  }
  
  getsinglecake(cake) {
    return this.http.get('/api/getcakes/' + cake);
    }
}
