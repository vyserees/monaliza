import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {CakeServiceService} from '../cake-service.service';

@Component({
  selector: 'app-cakelist',
  templateUrl: './cakelist.component.html',
  styleUrls: ['./cakelist.component.css']
})
export class CakelistComponent implements OnInit {

    public cakes;
    public cake;
  constructor(private cakeservice: CakeServiceService,
              private aroute: ActivatedRoute) { }

  ngOnInit() {
      this.cake = this.aroute.snapshot.paramMap.get('group');
      this.cakeservice.getcake(this.cake)
          .subscribe(data => this.cakes = data);
  }

}
