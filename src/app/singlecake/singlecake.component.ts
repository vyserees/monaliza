import { Component, OnInit } from '@angular/core';
import { CakeServiceService } from '../cake-service.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-singlecake',
  templateUrl: './singlecake.component.html',
  styleUrls: ['./singlecake.component.css']
})
export class SinglecakeComponent implements OnInit {

    public single;
    public singlecake;
    public cake;
    public tastes;
    public weight;
    public ukus = [];


    onKey(value) {
      this.weight += value;
    }
    getTaste(something) {
        this.http.get('/api/gettaste/' + something).subscribe(data => this.ukus.push(data));
    }
    removeTaste(value) {
        this.ukus.splice(value, 1);
    }

  constructor(private service: CakeServiceService,
              private route: ActivatedRoute,
              private http: HttpClient) { }

  ngOnInit() {
      this.cake = this.route.snapshot.paramMap.get('cake');
      this.service.getsinglecake(this.cake)
          .subscribe(data => this.singlecake = data);
      this.http.get('/api/gettaste').subscribe(data => this.tastes = data);
      this.http.get('/api/getprice/' + this.cake).subscribe(data => this.single = data);

  }

}
