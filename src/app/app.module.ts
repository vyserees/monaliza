import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { MeniComponent } from './meni/meni.component';
import { SinglecakeComponent } from './singlecake/singlecake.component';
import {CakeServiceService} from './cake-service.service';
import { CakelistComponent } from './cakelist/cakelist.component';


@NgModule({
  declarations: [
    AppComponent,
    MeniComponent,
    SinglecakeComponent,
    CakelistComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
      NgbModule.forRoot(),
      AppRoutingModule
  ],
  providers: [CakeServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
