import { TestBed, inject } from '@angular/core/testing';

import { CakeServiceService } from './cake-service.service';

describe('CakeServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CakeServiceService]
    });
  });

  it('should be created', inject([CakeServiceService], (service: CakeServiceService) => {
    expect(service).toBeTruthy();
  }));
});
